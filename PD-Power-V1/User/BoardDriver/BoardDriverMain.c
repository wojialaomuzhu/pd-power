#include "BoardDriverMain.h"
#include "main.h"

#include "LEDConfig.h"
#include "KeyConfig.h"
#include "OLEDConfig.h"
#include "Timer.h"



/*************************************************************
** Function name:      BoardDriverMain_Init
** Descriptions:       外设初始化
** Input parameters:   none
** Output parameters:  none
** Returned value:     none
** Created by:         none
** Created date:       none
*************************************************************/
void BoardDriverMain_Init(void)
{
	//HAL_Delay(2000);
	LEDConfig_Init();
	KeyConfig_Init();


}

/*************************************************************
** Function name:      BoardDriverMain_Loop
** Descriptions:       外设循环
** Input parameters:   none
** Output parameters:  none
** Returned value:     none
** Created by:         none
** Created date:       none
*************************************************************/

void BoardDriverMain_Loop(void)
{
	//打印调试信息
	RUN_BY_LIMIT_BLOCK(20,

	)

	LEDConfig_Loop();
	KeyConfig_Loop();
	//OLEDConfig_Loop();
}



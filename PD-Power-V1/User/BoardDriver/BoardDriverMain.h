/***********************************************************************
 * @file BoardDriverMain.h
 * Description:
 *
 **********************************************************************
 * File name:      BoardDriverMain.h
 * Date:           2020-10-03
 * Version:        V1.0
 * Author          liuxiang
 * @history:
 * V1.0 创建文件
***********************************************************************/
#ifndef __BoardDriverMain_h__
#define __BoardDriverMain_h__

#include "stdint.h"

void BoardDriverMain_Init(void);
void BoardDriverMain_Loop(void);

#endif // __BoardDriverMain_h__


/***********************************************************************
* @file PowerGPIO
* Description:
*
**********************************************************************
* File name:      PowerGPIO.h
* Date:           2022-12-06
* Version:        V1.0
* Author          liuxiang
* @history:
* V1.0 创建文件
***********************************************************************/
#ifndef __PowerGPIO_H__
#define __PowerGPIO_H__
#include "stdint.h"


void PowerGPIOInit(void);
void SetPowerIsEnable(uint8_t isEnable);
void SetPowerOutputVoltage(uint8_t outputVoltage);



#endif //__PowerGPIO_H__





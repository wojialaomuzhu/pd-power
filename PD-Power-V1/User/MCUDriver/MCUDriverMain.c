 #include "MCUDriverMain.h"
#include "main.h"

//定时任务函数
#include "timer.h"

//LED引脚
#include "LEDGPIO.h"

//电源引脚
#include "PowerGPIO.h"
/*************************************************************
** Function name:      MCUDriverMain_Init
** Descriptions:       芯片初始化
** Input parameters:   none
** Output parameters:  none
** Returned value:     none
** Created by:         none
** Created date:       none
*************************************************************/
void MCUDriverMain_Init(void)
{
	PowerGPIOInit();
}

/*************************************************************
** Function name:      MCUDriverMain_Loop
** Descriptions:       芯片循环
** Input parameters:   none
** Output parameters:  none
** Returned value:     none
** Created by:         none
** Created date:       none
*************************************************************/

void MCUDriverMain_Loop(void)
{
	//故障检测
	RUN_BY_LIMIT_BLOCK(1000,
		static uint8_t flag_out_is_enable = 0;
		if (flag_out_is_enable == 0) {
			SetPowerIsEnable(flag_out_is_enable);
			flag_out_is_enable = 1;
		} else if (flag_out_is_enable == 1){
			SetPowerIsEnable(flag_out_is_enable);
			flag_out_is_enable = 0;
		}
	)

}



#include "PowerGPIO.h"

#include "main.h"



/*************************************************************
** Function name:       PowerGPIOInit
** Descriptions:        初始化Power引脚
** Input parameters:    None
** Output parameters:   None
** Returned value:      None
** Remarks:             None
*************************************************************/
void PowerGPIOInit(void)
{
    SetPowerOutputVoltage(20);
    SetPowerIsEnable(0);
}
/*************************************************************
** Function name:      SetPowerIsEnable
** Descriptions:       设置输出使能
** Input parameters:   isEnable:  0:失能 1:使能
** Output parameters:  none
** Returned value:     none
** Created by:         none
** Created date:       none
*************************************************************/
void SetPowerIsEnable(uint8_t isEnable)
{
    if (isEnable) {
        HAL_GPIO_WritePin(OUT_EN_GPIO_Port, OUT_EN_Pin, 0);
    } else {
        HAL_GPIO_WritePin(OUT_EN_GPIO_Port, OUT_EN_Pin, 1);
    }
}


/*************************************************************
** Function name:      SetPowerOutputVoltage
** Descriptions:       设置输出电压
** Input parameters:   outputVoltage: 输出电压 9/12/15/20
** Output parameters:  none
** Returned value:     none
** Created by:         none
** Created date:       none
*************************************************************/
void SetPowerOutputVoltage(uint8_t outputVoltage)
{
    switch (outputVoltage)
    {
        case 9:
            HAL_GPIO_WritePin(PD_CFG_1_GPIO_Port, PD_CFG_1_Pin, 0);
            HAL_GPIO_WritePin(PD_CFG_2_GPIO_Port, PD_CFG_2_Pin, 0);
            HAL_GPIO_WritePin(PD_CFG_3_GPIO_Port, PD_CFG_3_Pin, 0);
            break;
        case 12:
            HAL_GPIO_WritePin(PD_CFG_1_GPIO_Port, PD_CFG_1_Pin, 0);
            HAL_GPIO_WritePin(PD_CFG_2_GPIO_Port, PD_CFG_2_Pin, 0);
            HAL_GPIO_WritePin(PD_CFG_3_GPIO_Port, PD_CFG_3_Pin, 1);
            break;
        case 15:
            HAL_GPIO_WritePin(PD_CFG_1_GPIO_Port, PD_CFG_1_Pin, 0);
            HAL_GPIO_WritePin(PD_CFG_2_GPIO_Port, PD_CFG_2_Pin, 1);
            HAL_GPIO_WritePin(PD_CFG_3_GPIO_Port, PD_CFG_3_Pin, 1);
            break;
        case 20:
            HAL_GPIO_WritePin(PD_CFG_1_GPIO_Port, PD_CFG_1_Pin, 0);
            HAL_GPIO_WritePin(PD_CFG_2_GPIO_Port, PD_CFG_2_Pin, 1);
            HAL_GPIO_WritePin(PD_CFG_3_GPIO_Port, PD_CFG_3_Pin, 0);
            break;
        default:
            break;
    }
}






